    # both fcs are to be used in file Scenarii/S_GreenBeard.py in Evolife
    
    def interaction(self, indiv, partner):
        if (indiv.gene_value('green')):
            if(partner.gene_value('green')):
                indiv.score((-1)*(self.Parameter('GB_Cost')))
                partner.score(self.Parameter('GB_Gift'))

        if (indiv.gene_value('fakeGreen')):
            if (partner.gene_value('fakeGreen')):
                if (random.randint(0, 1) == 0):
                    indiv.score(self.Parameter('GB_Cost'))
                else:
                    indiv.score((-1) * (self.Parameter('GB_Cost')))
                    partner.score(self.Parameter('GB_Gift'))

        def display_(self):
        return [('green1', 'green'), ('red', 'fakeGreen')]

            
