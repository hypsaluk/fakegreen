    def interaction(self, indiv, partner):
        gain = 20
        allowPrintInfo = False # prints info about state, but printing makes the sim slower

        # evaluate, if met two subjects with one of the allele (good/bad/tittat)
        if(
                ( # if individual has one and just one allel from the 3 options
                    (indiv.gene_value('tittat') and not indiv.gene_value('good') and not indiv.gene_value('bad'))
                    or
                    (not indiv.gene_value('tittat') and indiv.gene_value('good') and not indiv.gene_value('bad'))
                    or
                    (not indiv.gene_value('tittat') and not indiv.gene_value('good') and indiv.gene_value('bad'))
                )
            and
                ( # if partner has one and just one allel from the 3 options
                    (partner.gene_value('tittat') and not partner.gene_value('good') and not partner.gene_value('bad'))
                    or
                    (not partner.gene_value('tittat') and partner.gene_value('good') and not partner.gene_value('bad'))
                    or
                    (not partner.gene_value('tittat') and not partner.gene_value('good') and partner.gene_value('bad'))
                )
        ): # go further, if both subjects had just one allele
            info = "\n"
            info += "New meeting\n"
            if hasattr(indiv, 'cooperated'):
                info += "indiv cooperated before: " + str(indiv.cooperated) + "\n"
            else:
                info += "first time for indiv" + "\n"
            if hasattr(partner, 'cooperated'):
                info += "partner cooperated before: " + str(partner.cooperated) + "\n"
            else:
                info += "first time for partner" + "\n"

            # depending on indiv strategy, choose if he will cooperate
            indivCoop = True
            if(indiv.gene_value('bad')):
                indivCoop = False
            elif(indiv.gene_value('tittat')):
                # TODO forgivness; forgivness = ((random.randint(0, 1)) == 0)
                if hasattr(partner, 'cooperated'):
                    indivCoop = partner.cooperated
                else:
                    indivCoop = True
                info += "indiv is tittat; his cooperaion: " + str(indivCoop) + "\n"

            # depending on partner strategy, choose if he will cooperate
            partnerCoop = True
            if (partner.gene_value('bad')):
                partnerCoop = False
            elif (partner.gene_value('tittat')):
                # TODO forgivness; forgivness = ((random.randint(0, 1)) == 0)
                if hasattr(indiv, 'cooperated'):
                    partnerCoop = indiv.cooperated
                else:
                    partnerCoop = True
                info += "partner is tittat; his cooperaion: " + str(partnerCoop) + "\n"

            # set if they cooperated, for next time
            indiv.cooperated = indivCoop
            partner.cooperated = partnerCoop

            # evaluate result of both (non)coordination
            if(indivCoop and partnerCoop): # both cooperated
                indiv.score(gain/2)
                partner.score(gain/2)
            elif(not indivCoop and partnerCoop): # indiv betrayed -> indiv wins all
                indiv.score(gain)
                partner.score(0)
            elif(indivCoop and not partnerCoop): # partner betrayed -> partner wins all
                indiv.score(0)
                partner.score(gain)
            else: # both didnt cooperate -> both lost
                indiv.score(0)
                partner.score(0)

            if allowPrintInfo:
                print(info)

    def display_(self):
        return [('green1', 'good'), ('red', 'bad'), ('blue', 'tittat')]